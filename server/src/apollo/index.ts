import express from "express";
import { ApolloServer } from "apollo-server-express";
import { makeAugmentedSchema, assertSchema } from "neo4j-graphql-js";

import { getDriver } from "../utils/neo4j";
import typeDefs from "./typeDefs"
import resolvers from "./resolvers";
import { getUserByToken } from "./resolvers/auth"

import {authenticateRequest} from "./resolvers/auth"

const driver = getDriver();

const schema = makeAugmentedSchema({
    typeDefs,
    resolvers,
    config: {
        mutation: false
    }
});

assertSchema({ schema, driver });

const apolloServer = new ApolloServer({
    schema,
    // inject the request object into the context to support middleware
    // inject the Neo4j driver instance to handle database call
    context: async ({ req, res }) => {
        return {
            driver,
            req,
            res,
            ...await authenticateRequest(req,driver,res)
        };
    },
});

export const applyApolloMiddleware = (app: express.Application) => {
    apolloServer.applyMiddleware({ app, path: process.env.GRAPH_ENDPOINT });
}
