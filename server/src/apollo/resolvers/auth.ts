import { AuthenticationError } from "apollo-server-express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const JWT_SECRET = process.env.JWT_SECRET;
const JWT_REFRESH_SECRET = process.env.JWT_REFRESH_SECRET;

const createRefreshToken = async (userId, driver, res) => {
    const refreshToken = jwt.sign(
        { id: userId },
        JWT_REFRESH_SECRET,
        { expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRES });

    await driver.session().run(`
    MATCH (u:User { id: '${userId}' })-[:REFRESH_TOKEN]->(ort:RefreshToken)
    DETACH DELETE ort
    `);

    await driver.session().run(`
    MATCH (u2:User { id: '${userId}' })
    CREATE (rt:RefreshToken { token: '${refreshToken}' })
    CREATE (u2)-[:REFRESH_TOKEN]->(rt)
    `);

    res.cookie('refreshtokencookie', refreshToken, { path: '/', maxAge: process.env.COOKIE_MAX_AGE, httpOnly: true })
    return refreshToken;
}

const createToken = (userId, driver, res) => {
    const token = jwt.sign(
        { id: userId },
        JWT_SECRET,
        { expiresIn: process.env.JWT_EXPIRES });
    res.cookie('authcookie', token, { path: '/', maxAge: process.env.COOKIE_MAX_AGE, httpOnly: true })
    return token;
}

export const authenticateRequest = async (req, driver, res) => {
    let token = req.cookies.authcookie;
    if (!token) return {}
    jwt.verify(token, JWT_SECRET, async (err, decoded) => {
        if (err) {
            console.log(err)
            if (err.message == "jwt expired") {
                const refreshtoken = req.cookies.refreshtokencookie;
                if (!refreshtoken) return {}

                jwt.verify(refreshtoken, JWT_REFRESH_SECRET, async (err, decoded) => {
                    if (err) {
                        return {}
                    }
                    token = createToken(decoded.id, driver, res);
                    const user = await getUserByToken(token, driver);
                    return {
                        token,
                        user
                    }
                });
            }
            return {};
        }
    })
    const user = await getUserByToken(token, driver);
    return {
        token,
        user
    }
}

export const getUserByToken = async (token, driver) => {
    const decoded: any = jwt.verify(token, JWT_SECRET, { ignoreExpiration: true });
    const user = await driver
        .session()
        .run(`MATCH (a:User {id: "${decoded.id}"}) RETURN a`);
    return user.records[0]?.get(0).properties;
};

export default {
    Query: {

    },
    Mutation: {
        async signup(parent, args, context, info) {
            const hashedPassword = await bcrypt.hash(args.password, 10);
            const user = await context.driver
                .session()
                .run(
                    `CREATE (a:User { id: apoc.create.uuid(), name: "${args.name}",email: "${args.email}", password: "${hashedPassword}"}) RETURN a`
                );

            const refreshToken = await createRefreshToken(user.id, context.driver, context.res);
            const token = createToken(user.id, context.driver, context.res);

            const { password, ...rest } = user.records[0].get(0).properties;
            return rest;
        },
        async login(parent, args, context, info) {
            const result = await context.driver
                .session()
                .run(`MATCH (a:User {email: "${args.email}"}) RETURN a`);
            if (!result.records[0]) throw new AuthenticationError("Invalid username or password");

            const user = result.records[0].get(0).properties;

            const valid = await bcrypt.compare(
                args.password,
                user.password
            );

            if (!valid) throw new AuthenticationError("Invalid username or password");

            const refreshToken = await createRefreshToken(user.id, context.driver, context.res);
            const token = createToken(user.id, context.driver, context.res);

            return token;
        },
        async logout(parent, args, context, info) {
            await context.driver.session().run(`
            MATCH (u:User { id: '${context.user.id}' })-[:REFRESH_TOKEN]->(ort:RefreshToken)
            DETACH DELETE ort
            `);

            context.res.cookie('refreshtokencookie', "", { path: '/', maxAge: 1, httpOnly: true })
            context.res.cookie('authcookie', "", { path: '/', maxAge: 1, httpOnly: true })
            return "ok";
        },

        async me(parent, args, context, info) {
            const user = await getUserByToken(args.token, context.driver);
            if (!user) return "Invalid user";
            const { password, ...rest } = user;
            return rest;
        }
    }
}

