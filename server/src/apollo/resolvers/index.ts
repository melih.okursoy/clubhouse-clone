import { AuthenticationError } from "apollo-server-express";
import { neo4jgraphql } from "neo4j-graphql-js";

import auth from "./auth";

const resolvers = {
    Query: {
        User(object, params, ctx, resolveInfo) {
             if (!ctx.user) throw new AuthenticationError("request not authenticated");
            return neo4jgraphql(object, params, ctx, resolveInfo);
        },
        Room(object, params, ctx, resolveInfo) {
             if (!ctx.user) throw new AuthenticationError("request not authenticated");
            return neo4jgraphql(object, params, ctx, resolveInfo);
        },
        ...auth.Query
    },
    Mutation: {
        ...auth.Mutation
    }
}

export default resolvers;