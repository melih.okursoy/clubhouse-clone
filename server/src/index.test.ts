import request from 'supertest';
import  "@types/jest"
import app from "./index";

beforeAll(() => {
    process.env.NODE_ENV = 'test';
})
describe('index', () => {

    test('app should be defined', () => {
        expect(app).toBeDefined();
    })

    test('root should respond', async () => {
        const res = await request(app).get('/');
        expect(res.status).toBe(200);
        expect(res.body).toMatchSnapshot();
    })

})

