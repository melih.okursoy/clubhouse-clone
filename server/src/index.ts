import express, { Application } from 'express';
import bodyParser from 'body-parser';
import cors from "cors";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";

dotenv.config();

import { applyApolloMiddleware } from "./apollo";

const app: Application = express();
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors({
  // origin: 'http://localhost:8080',
  // methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  credentials: true
}));

// function sampleMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
//   console.log(request.path,request.method, request.cookies.authcookie) 
//   next();
// }

//app.use(sampleMiddleware);

app.get("/", (req: express.Request, res: express.Response) => {
  res
    .status(200)
    .send("Express + TypeScript Server");
});

applyApolloMiddleware(app)

app.listen(process.env.PORT, () => {
  console.log(`⚡️[neo4j]: Server is running at http://localhost:${process.env.NEO4J_BROWSER_PORT}`);
  console.log(`⚡️[server]: Server is running at http://localhost:${process.env.PORT}`);
  console.log(`⚡️[graph]: Server is running at http://localhost:${process.env.PORT}${process.env.GRAPH_ENDPOINT}`);
});

export default app;
